# Supernovae Detection
## About
This is the main working repository for our scholarship-awarded supernovae
detection graduation project, which is about detection of supernovae using
state-of-the-art machine learning models.

## Training
TODO: Document training of all trained models.

## Roadmap
TODO: Design the roadmap.

## License
Copyright (C) 2022-2023 Abdullah Çırağ, İkra Nergiz  
Supernova Detection is distributed under the terms of the GNU General Public
License as published by the Free Software Foundation, version 3. A copy of this
license can be found in the file COPYING included with the source code of this
program. If not, see <https://www.gnu.org/licenses/>.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
