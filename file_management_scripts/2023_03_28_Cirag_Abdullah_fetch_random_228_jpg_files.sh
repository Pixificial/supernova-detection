#!/bin/sh

IMGFILES=$(ls | shuf | sed 228q)
LBLFILES=$(echo $IMGFILES | tr "jpg" "txt" | tr "\ " "\n" | sed -e "s/^/..\/labels\//")

cp $IMGFILES ../tmp/images/
cp $LBLFILES ../tmp/labels/
