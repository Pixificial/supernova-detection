#!/bin/sh

IMGFILES=$(ls | shuf | sed 800q)
LBLFILES=$(echo $IMGFILES | tr "png" "txt" | tr "\ " "\n" | sed -e "s/^/..\/labels\//")

cp $IMGFILES ../tmp/images/
cp $LBLFILES ../tmp/labels/
