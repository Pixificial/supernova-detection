#!/bin/sh

python3 train.py --workers 8 --device cpu --batch-size 16 --epochs 240 --data /home/vdude/projects/psp_ori/data.yaml --hyp /home/vdude/git/yolov7/data/hyp.scratch.custom.yaml --cfg /home/vdude/git/yolov7/cfg/training/yolov7-tiny.yaml --weights /home/vdude/projects/psp_ori/yolov7_training.pt --name supernova-psp-ori-train

